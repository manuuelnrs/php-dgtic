<!-- Juan Manuel Nava Rosales -->
<?php
session_start();

if($_SESSION['Entrar']=='False'){
  header('Location: login.php');
  exit;
}

if(isset($_POST['Registrar'])){
  $numero_cuenta = $_POST['num_cta'];
  $contrasena = $_POST['contrasena'];
  $nombre = $_POST['nombre'];
  $primer_ap = $_POST['primer_apellido'];
  $segundo_ap = $_POST['segundo_apellido'];
  $fec_nac = new DateTime($_POST['fec_nac']);
  $contrasena = $_POST['contrasena'];
  $genero = $_POST['genero'];
  $_SESSION['Alumno'][(string)$numero_cuenta] = [$numero_cuenta,$nombre,$primer_ap,$segundo_ap,$fec_nac->format('d/m/Y'),$contrasena,$genero];
  header('Location: info.php');
  exit;
}

?>

<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="author" content="Juan Manuel Nava Rosales">
    <meta name="description" content="Sitio Web de Formulario">
    <meta name="keywords" content="Login, HTML, CSS, PHP">
    <title>Formulario</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/formulario.css">
  </head>

  <body class="bg-light">

    <nav class="nav">
      <ul class="nav-ul">
        <li class="nav-item">
          <a class="nav-link active" href="info.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="formulario.php">Registrar Alumnos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="cerrar.php">Cerrar Sesión</a>
        </li>
      </ul>
    </nav>

    <main role="main" class="container">
      <div class="my-3 p-3 bg-light rounded shadow-sm">
        <div class="container col-md-10">
          <form id="form-registro" method="POST">
            <div class="form-group">
              <label for="num_cta">Número de Cuenta</label>
              <input type="number" class="form-control" id="num_cta" name="num_cta" placeholder="Número de Cuenta" require>
            </div>
            <div class="form-group">
              <label for="nombre">Nombre</label>
              <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" require>
            </div>
            <div class="form-group">
              <label for="primer_apellido">Primer Apellido</label>
              <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" placeholder="Primer Apellido" require>
            </div>
            <div class="form-group">
              <label for="segundo_apellido">Segundo Apellido</label>
              <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" placeholder="Segundo Apellido" >
            </div>
            <div class="form-group">
              <label for="from-control">Género</label><br>
              <input type="radio" id="genero_hombre" name="genero" value="M">
              <label for="from-control">Hombre</label><br>
              <input type="radio" id="genero_mujer" name="genero" value="F">
              <label for="from-control">Mujer</label><br>
              <input type="radio" id="genero_otro" name="genero" value="O">
              <label for="from-control">Otro</label><br>
            </div>
            <div class="form-group">
              <label for="fec_nac">Fecha de Nacimiento</label>
              <input type="date" class="form-control" id="fec_nac" name="fec_nac">
            </div>
            <div class="form-group">
              <label for="contrasena">Contraseña</label>
              <input type="password" class="form-control" id="contrasena" name="contrasena"  placeholder="Contraseña">
            </div>

            <button type="submit" class="btn btn-primary" title="Registrar" name="Registrar" value="Registrar">Registrar</button>
          </form>
        </div>        
      </div>
    </main>

  </body>
</html>