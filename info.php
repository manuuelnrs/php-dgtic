<!-- Juan Manuel Nava Rosales -->
<?php
session_start();

if($_SESSION['Entrar']=='False'){
  header('Location: login.php');
  exit;
} else{
  $num_cta = $_SESSION['Alumno'][$_SESSION['ID_Entrar']][0];
  $nombre = $_SESSION['Alumno'][$_SESSION['ID_Entrar']][1];
  $primer_apellido = $_SESSION['Alumno'][$_SESSION['ID_Entrar']][2];
  $segundo_apellido = $_SESSION['Alumno'][$_SESSION['ID_Entrar']][3];
  $fec_nac = $_SESSION['Alumno'][$_SESSION['ID_Entrar']][4];
  $contrasena = $_SESSION['Alumno'][$_SESSION['ID_Entrar']][5];
  $genero = $_SESSION['Alumno'][$_SESSION['ID_Entrar']][6];
}
?>

<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="author" content="Juan Manuel Nava Rosales">
    <meta name="description" content="Sitio Web de Formulario">
    <meta name="keywords" content="Login, HTML, CSS, PHP">
    <title>Formulario</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/formulario.css">
    <link rel="stylesheet" href="css/info.css">
  </head>

  <body class="bg-light">

    <nav class="nav">
      <ul class="nav-ul">
        <li class="nav-item">
          <a class="nav-link active" href="info.php">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="formulario.php">Registrar Alumnos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="cerrar.php">Cerrar Sesión</a>
        </li>
      </ul>
    </nav>

    <main role="main" class="container">
      <div class="my-3 p-3 bg-light rounded shadow-sm">
        <h1>Usuario Autenticado</h1>
        <div class="container col-md-12">
          <img src="https://i.pravatar.cc/64">
          <?php echo $nombre." ".$primer_apellido;?>
          <hr><h4>Información</h4>
          <?php echo "Número de cuenta: ".$num_cta."<br>";?>
          <?php echo "Fecha de Nacimiento: ".$fec_nac."<br>";?>
        </div>        
      </div>

      <div class="my-3 p-3 bg-light rounded">
        <h1>&nbsp;Datos guardados&nbsp;</h1>
        <div class="container col-md-12">
          <table class="styled-table"> 
            <thead><tr><th>#</th><th>Nombre</th><th>Fecha de Nacimiento</th></tr></thead> 
            <tbody id="datos_guardados">
              <?php 
                foreach($_SESSION['Alumno'] as $llave => $valor){
                  echo "<tr> <td>".$valor[0]."</td><td>".$valor[1]."</td><td>".$valor[4]."</td> </tr>";
                }
              ?>
            </tbody> 
          </table>
        </div>        
      </div>
    </main>

  </body>
</html>