<!-- Juan Manuel Nava Rosales -->
<?php
// Example
// $reg = "01234567890123456789012345678900";
// $x = preg_match('/^((.){1,30}(\s+(.){1,30})*)$/i', $reg);


//Realizar una expresión regular que detecte emails correctos.
$email = "dsds@gmail.com";
$b = preg_match('/[\w._%+]+@[\w.-]+\.[a-zA-Z]{2,4}/', $email);
echo "Email: ".$b."\n";


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$CURP = "ABCD123456EFGHIJ78";
$c = preg_match('/^[A-D]{4}[1-6]{6}[E-J]{6}[7|8]{2}$/', $CURP);
echo "CURP: ".$c."\n";


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$cadena50 = "ABCDEFHIJLGOQWERTYUIOPASDFGHJKLZXCMVNQWERTYUIOPASDF";
$d = preg_match('/^[A-Za-z]{50,}$/', $cadena50);
echo "CADENA50: ".$d."\n";


//Crea una funcion para escapar los simbolos especiales.
$especiales = "!#%$%$/()";
$e = preg_match('/[\W]+/', $especiales);
echo "SimbolosEspeciales: ".$e."\n";

//Crear una expresion regular para detectar números decimales.
$decimales = "2.0";
$f = preg_match('/^[0-9]{1,3}\.[0-9]{1,3}$/', $decimales);
echo "Decimales: ".$f."\n";


?>