<!-- Juan Manuel Nava Rosales -->
<?php
session_start();
$_SESSION['Alumno']['1'] = [1,'Admin','General','','25/01/1990','adminpass123.','O'];

if(isset($_POST['Entrar'])){
  $numero_cuenta = $_POST['cuenta'];
  $contrasena = $_POST['password'];

  if(!empty($_SESSION['Alumno'])) {
    foreach($_SESSION['Alumno'] as $llave => $valor){
      if( $numero_cuenta==$valor[0] and $contrasena==$valor[5] ){
        $_SESSION['Entrar'] = 'True';
        $_SESSION['ID_Entrar'] = $valor[0];
        header('Location: info.php');
        exit;
      }
      // Aquí me falto (time) agregar la excepción por error de datos de inicio de sesión
    }
  }
  else{ 
    echo '<script type="text/javascript">alert("¡Error en los datos de Inicio de Sesión!");</script>';
  }
  
}

?>

<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="author" content="Juan Manuel Nava Rosales">
    <meta name="description" content="Sitio Web de Formulario">
    <meta name="keywords" content="Login, HTML, CSS, PHP">
    <title>Login</title>   
    
    <link rel="stylesheet" href="css/login.css">
  </head>
  
  <body>
    <div id="contenedor">
      <div id="contenedorcentrado">
        <div id="login">
          <form id="loginform" method="POST">
              <label for="cuenta">Numero de Cuenta</label>
              <input id="cuenta" type="text" name="cuenta" placeholder="Numero de Cuenta" required>
              
              <label for="password">Contraseña</label>
              <input id="password" type="password" placeholder="Contraseña" name="password" required>
              
              <button type="submit" title="Entrar" name="Entrar" value="Entrar">Entrar</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>