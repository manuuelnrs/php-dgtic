Juan Manuel Nava Rosales - Investigación de Funciones PHP:
substr : devuelve parte de cadena (cadena, pos_inicio, longitud)
strstr : devueve aparicion de cadena (cadena, str_coincidencia, inv_aguja) (sensible a mayús)
strpos : encuentra posición de coincidencia (cadena, coincidencia, offset)
implode : une array en una cadena (separador, array)
explode : divide cadena en elementos de array (delimitador, cadena, limite)
utf8_encode : Convierte una cadena de ISO-8859-1 (Latín 1) a UTF-8 (cadena)
utf8_decode :  Convierte un string desde UTF-8 a ISO-8859-1 (cadena)
array_pop : extrar último elemento de array (array)
array_push : Insertar uno o + elementos al final del array (array, elemento...s)
array_diff : deuvleve array con diferencia entre arrays (array, array...s)
array_walk : aplica función a cada miembro de un array (array, callback, userdata NULL)
sort : ordenar array (array, sort_regular) : bool
current (top) : devuelve el elemento actual en un array (array)
date : dar formato a fecha local (formato'', timestamp = time()) 
empty : vacío : bool
isset : establecido
serialize : representación para el almacenamiento de un valor ( valor )
unserialize : crea valor PHP a partir de representación almacenada (cadena, opciones)